#!/bin/bash

if [[ -e /etc/init.d/petclinic ]]
then
	service petclinic stop
fi

# echo "Installing necessary software"
# yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm
# yum -y install epel
# yum -y install mysql157-server
# yum -y install java-1.8.0-openjdk
# yum -y install mariadb-server git mvn java-1.8.0-openjdk mariadb java-1.8.0-openjdk-devel

#echo "Downloading or updating the petclinic code"
#cd /opt
#if ! git clone https://github.com/spring-projects/spring-petclinic.git
#then
#	cd spring-petclinic
#	git pull
#fi

# mkdir -p /opt/spring-petclinic
# echo "Copying Files from Bucket"
# aws s3 cp s3://vazbucket/meme.zip /opt/spring-petclinic/meme.zip
# aws s3 cp s3://vazbucket/spring-petclinic-2.0.0.jar /opt/spring-petclinic/
#
# echo "Unzipping Copied files"
# cd /opt/spring-petclinic
# unzip meme.zip
# rm -rf meme.zip

#echo "Starting the Database server"
#systemctl enable mariadb
#systemctl start mariadb

# Configure petclinic application
#mysql -u root -e "create database petclinic;"
#mysql -u root -e "create user 'root'@'%' identified by 'petclinic';"
#mysql -u root -e "grant all on petclinic.* to 'root'@'%';"
#mysql -u root -e "grant all on petclinic.* to 'root'@'localhost';"

# Create start/stop script
cat >/etc/init.d/petclinic <<_END_
#!/bin/bash

#description: Petclinic control script
#chkconfig: 2345 99 99

case \$1 in
  'start')
    # The next 2 lines is only required if you want to compile and run
    #cd /opt/spring-petclinic
    #./mvnw spring-boot:run >/var/log/petclinic.stdout 2>/var/log/petclinic.stderr &
    # The next 2 lines are for running PC from a pre-compiled jar
    cd /opt/petclinic
    java -jar /opt/petclinic/spring-petclinic-2.0.0.jar >/var/log/petclinic.stdout 2>/var/log/petclinic.stderr &
    ;;
  'stop')
    kill \$(ps -ef | grep petclinic | grep -v grep | awk '{print \$2}')
    ;;
  'status')
    PID=\$(ps -ef | grep java | grep petclinic | grep -v grep | awk '{print \$2}')
    if [[ -n \$PID ]]
    then
      echo "Petclinic is running with PID \$PID"
    fi
    ;;
  *)
    echo "I do not understand that option"
    ;;
esac
_END_

>/var/log/petclinic.stdout
>/var/log/petclinic.stderr

chmod +x /etc/init.d/petclinic
chkconfig --add petclinic
echo "Starting PetClinic"
service petclinic start

until grep "Started PetClinicApplication in .* seconds" /var/log/petclinic.std* >/dev/null 2>&1
do
	sleep 20
done
echo "System ready"
