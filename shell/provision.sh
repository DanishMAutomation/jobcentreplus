#!/bin/bash

echo "Installing necessary software"
yum -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm
yum -y install epel
yum -y install mysql157-server
yum -y install java-1.8.0-openjdk
yum -y erase java-1.7.0-openjdk
yum -y install java-1.8.0-openjdk-devel

mkdir -p /opt/spring-petclinic

echo "Copying Files from Bucket"
aws s3 cp s3://vazbucket/spring-petclinic-2.0.0.jar /opt/spring-petclinic/

#echo "Unzipping Copied files"
#cd /opt/spring-petclinic
#unzip file.zip
#rm -rf file.zip
#unzip meme.zip
#rm -rf meme.zip
